#!env python
"""
Guess Who Game v0.1

Usage:
	guesswho new_game <num_pics>
	guesswho show_board
	guesswho gender (male|female)
	guesswho has (eyeglasses|sunglasses|beard|mustache|eyes_open|mouth_open|smile)
	guesswho solve <character>

Options:
	new_game	Resets and initializes the board for a new game
	show_board	Prints a representation of the current game
	gender		Tells you which pictures represent (male|female) people
	has		Returns a list of pictures which actually have the specified attribute
	solve		Specify the character who solves the game

	-h --help     	Show this screen.
	-v --version	Show version.
"""

import docopt
import commands
from inspect import getmembers, isclass

if __name__ == "__main__":

	try:
		#Parse arguments
		arguments = docopt.docopt(__doc__, version='Guess Who Game v0.1')
		#print arguments

		for k, v in arguments.iteritems():
			if hasattr(commands, k) and v == True:
				module = getattr(commands, k)
				commands = getmembers(module, isclass)
				command = [command[1] for command in commands if command[0] != 'Base'][0]
				command = command(arguments)
				command.run()

	except docopt.DocoptExit as e:
		print e.message
