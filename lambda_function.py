import boto3
import json

def lambda_handler(event, context):
    
    for event in event["Records"]:
            
        fileName=event["s3"]["object"]["key"]
        bucket=event["s3"]["bucket"]["name"]
        
        cli_rek=boto3.client('rekognition','eu-west-1')
    
        response_face = cli_rek.detect_faces(Image={'S3Object':{'Bucket':bucket,'Name':fileName}},Attributes=['ALL'])
        response_label = cli_rek.detect_labels(Image={'S3Object':{'Bucket':bucket,'Name':fileName}},MinConfidence=30)
        
        cli_s3=boto3.client('s3','eu-west-1')
        
        cli_s3.put_object(
            Body=json.dumps(response_face, indent=4, sort_keys=True),
            Bucket=bucket,
            ContentType='Application/json',
            #Key="text/"+fileName.split("/")[1].split(".")[0]+"_facial_analysis.json"
            Key="text/"+fileName.split("/")[1]+"_facial_analysis.json"
        )

        cli_s3.put_object(
            Body=json.dumps(response_label, indent=4, sort_keys=True),
            Bucket=bucket,
            ContentType='Application/json',
            #Key="text/"+fileName.split("/")[1].split(".")[0]+"_label_analysis.json"
            Key="text/"+fileName.split("/")[1]+"_label_analysis.json"
        )
        
        # print('Detected faces for ' + fileName)
        # for faceDetail in response['FaceDetails']:
        #     print('The detected face is between ' + str(faceDetail['AgeRange']['Low'])
        #           + ' and ' + str(faceDetail['AgeRange']['High']) + ' years old')
        #     print('Here are the other attributes:')
        #     #print(json.dumps(faceDetail, indent=4, sort_keys=True))
    

    return 'Ok'

