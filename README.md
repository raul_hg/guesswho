# Guess The Cloudiest?

This repo contains the basic functionality for the MVP.

## Overview

The game is working as shown in the demo video. We created a complete CLI to interact with the images and videos.

## Start Playing

Nothing is needed to start the game. To get help about the game options, just execute the `guesswho.py` script.

We recommend to create a symlink on /usr/local/bin/, so the CLI will be clearer and system wide.

`ln -s ln -s <path_to_repo>/guesswho.py /usr/local/bin/guesswho`

# Basic Commands

- `guesswho --help`: Prints the long version of help.
- `guesswho start_game 10`: Initializes a game with 10 pictures.
- `guesswho show_board`: Shows a printable version of the current situation of the game. <Spoiler alert>, this command will show you who is the character to guess!
- `guesswho gender male|female`: Asks if the gender of the character to guess is male or female, It also gives you the option to delete the characters who doesn't match this criteria.
