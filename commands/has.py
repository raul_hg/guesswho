"""The has command."""

from json import dumps
from .base import Base
import json

attr_map = {
    "eyeglasses": "Eyeglasses",
    "sunglasses": "Sunglasses",
    "beard": "Beard",
    "mustache": "Mustache",
    "eyes_open": "EyesOpen",
    "mouth_open": "MouthOpen",
    "smile": "Smile"
}

class Has(Base):
    """This command says if the person has a concrete attribute"""

    def get_attr(self, attr):

        print "--- Looking for people with %s ---" % (attr)

        result = []

    	for player in game:
            data = json.load(open(self.game_dir+'text/'+player+'_facial_analysis.json'))
            for detail in data["FaceDetails"]:
                if detail[attr]['Confidence'] > 80:
                    result.append(player + "\t" + str(detail[attr]['Value']))
                else:
                    result.append(player + "\t" + str(not detail[attr]['Value']))

        return result

    def run(self):
        #print self.options
    	game, main = self.load_game()

        res = []
        attr = ""
        for k, v in self.options.iteritems():
            if k == 'eyeglasses' and v == True:
                attr = attr_map["eyeglasses"]
            elif k == 'sunglasses' and v == True:
                attr = attr_map["sunglasses"]
            elif k == 'beard' and v == True:
                attr = attr_map["beard"]
            elif k == 'mustache' and v == True:
                attr = attr_map["mustache"]
            elif k == 'eyes_open' and v == True:
                attr = attr_map["eyes_open"]
            elif k == 'mouth_open' and v == True:
                attr = attr_map["mouth_open"]
            elif k ==  'smile' and v == True:
                attr = attr_map["smile"]

        answ = False
        if attr:
            if self.person_has_attr(self.main, attr):
                print "Person %s has %s" % (self.main, attr)
                answ = self.ask("Do you want to discard the people who has not %s?" % attr)
                if answ:
                    self.remove_cards(attr, False)
            else:
                print "Person %s has not %s" % (self.main, attr)
                answ = self.ask("Do you want to discard the people who has %s?" % attr)
                if answ:
                    self.remove_cards(attr, True)
