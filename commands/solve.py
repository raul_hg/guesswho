"""The solve command."""
from .base import Base

class Solve(Base):
    """This command shows the board"""

    def run(self):

    	game, main = self.load_game()
        if self.main == "main_" + self.options["<character>"]:
            print "That's correct!! You win!!"
        else:
            print self.args
            print "Wrong guess... Try again!!"