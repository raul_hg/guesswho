from .gender import *
from .has import *
from .new_game import *
from .show_board import *
from .solve import *
