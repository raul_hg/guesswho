"""The show board command."""
from .base import Base

class ShowBoard(Base):
    """This command shows the board"""

    def run(self):

        self.load_game()

        print("")
        print ">>> MAIN CHARACTER <<<"
        print self.main

        print("")
        print(">>>>> BOARD <<<<<")

        for pic in self.game:
            print pic
            if self.person_has_attr(pic, "Male"):
                print(" - Male")
            else:
                print(" - Female")
            if self.person_has_attr(pic, "Eyeglasses"):
                print(" - Has eyeglasses")
            if self.person_has_attr(pic, "Sunglasses"):
                print(" - Has sunglasses")
            if self.person_has_attr(pic, "Beard"):
                print(" - Has beard")
            if self.person_has_attr(pic, "MouthOpen"):
                print(" - Has mouth open")
            if self.person_has_attr(pic, "EyesOpen"):
                print(" - Has eyes open")
            if self.person_has_attr(pic, "Mustache"):
                print(" - Has moustache")
            if self.person_has_attr(pic, "Smile"):
                print(" - Has smile")
