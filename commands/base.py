import glob
import json
import sys
import os

class Base(object):
    """A base command."""

    def __init__(self, options, *args, **kwargs):
        self.options = options
        self.args = args
        self.kwargs = kwargs

        self.game = []
        self.main = ""

        self.game_dir="/Users/raulherranz/git/guesswho/"
        self.max_num_pics=526
        self.s3_bucket="raulico"

    def load_game(self):
    	game = []
        main = ""
    	files = glob.glob(self.game_dir+"pics/*")

    	for file in files:
            if os.path.isfile(file):
                if file.split("/")[-1].startswith('main'):
                    self.main = file.split("/")[-1]
                else:
                    self.game.append(file.split("/")[-1])

    	return game, main

    def person_has_attr(self, person, attr):
        """Tells if a person has certain attribute"""

        data = json.load(open(self.game_dir+'text/'+person+'_facial_analysis.json'))

        for detail in data["FaceDetails"]:
            # Check gender
            if detail["Gender"]["Value"] == "Male" and attr == "Male":
                return True
            elif detail["Gender"]["Value"] == "Female" and attr == "Female":
                return True
            elif attr == "Male" or attr == "Female":
                return False
            else:
                return bool(detail[attr]['Value'])

            # Check the rest of the attributes
            # elif detail[attr]['Confidence'] > 90:
            #     return detail[attr]['Value']
            # else:
            #     return not detail[attr]['Value']

    def ask(self, question, default="yes"):
        """Ask a yes/no question via raw_input() and return their answer.

        "question" is a string that is presented to the user.
        "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

        The "answer" return value is True for "yes" or False for "no".
        """

        valid = {
            "yes": True, "y": True, "ye": True,
            "no": False, "n": False
        }
        if default is None:
            prompt = " [y/n] "
        elif default == "yes":
            prompt = " [Y/n] "
        elif default == "no":
            prompt = " [y/N] "
        else:
            raise ValueError("invalid default answer: '%s'" % default)

        while True:
            sys.stdout.write(question + prompt)
            choice = raw_input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                sys.stdout.write("Please respond with 'yes' or 'no' "
                                 "(or 'y' or 'n').\n")

    def remove_cards(self, attr, value):
        """Delete cards from the board"""

        for people in self.game:
            # If both are true or both are false
            # if self.person_has_attr(people, attr):
            #     print "%s has %s and value is %r" % (people, attr, value)
            # else:
            #     print "%s has not %s and value is %r" % (people, attr, value)

            # print "person has: " + str(type(self.person_has_attr(people, attr))) + " and value is " + str(value)
            if self.person_has_attr(people, attr) == value:
                print ">> Discarding %s..." % people
                os.rename("pics/"+people, "pics/discarded/"+people)
                #os.remove("text/"+people+"_facial_analysis.json", )
                #os.remove("text/"+people+"_label_analysis.json")

    def run(self):
        raise NotImplementedError('You must implement the run() method yourself!')
