"""The gender command."""

from json import dumps
from .base import Base
import glob
import json
import sys

class Gender(Base):
    """This command says if they're male or female"""

    def run(self):

        self.load_game()

        male = self.options["male"]
        female = self.options["female"]

        gender = "Male"
        if female:
            gender = "Female"

        answ = False
        delete = ""
        if self.person_has_attr(self.main, gender):
            if male: delete = "Female"
            elif female: delete = "Male"

            print "The person is %s!" % (gender)
            answ = self.ask("Do you want to discard the people who is not %s?" % gender)
        else:
            print "The person is NOT %s!" % (gender)
            delete = gender

            answ = self.ask("Do you want to discard the people who is %s?" % gender)

        if answ:
            self.remove_cards(delete, True)
