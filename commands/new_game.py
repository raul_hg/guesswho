"""The new_game command."""

from json import dumps
from .base import Base
import random
import os, shutil
import boto3


class NewGame(Base):
    """Start a new game"""

    def empty_game_dirs(self):
    	print("--- Cleaning folders ---")

    	folders = [self.game_dir+'pics/', self.game_dir+'text/']
    	for folder in folders:
    		for the_file in os.listdir(folder):
    		    file_path = os.path.join(folder, the_file)
    		    try:
    		        if os.path.isfile(file_path):
    		            os.unlink(file_path)
    		        #elif os.path.isdir(file_path): shutil.rmtree(file_path)
    		    except Exception as e:
    		        print(e)
		
		if not os.path.exists(self.game_dir+'pics/discarded/'):
			os.makedirs(self.game_dir+'pics/discarded/')


    def initialize_game(self, num_pics):
    	print("--- Initializing game ---")

    	game = []
    	pic = 0

    	while pic < num_pics:
    		game.append("cloudreacher"+str(int(random.random() * self.max_num_pics)))
    		pic = pic+1

    	s3 = boto3.resource('s3')

    	for pic in game:
    		s3.meta.client.download_file(self.s3_bucket, 'pics/'+pic, 'pics/'+pic)
    		s3.meta.client.download_file(self.s3_bucket, 'text/'+pic+"_facial_analysis.json", 'text/'+pic+"_facial_analysis.json")
    		s3.meta.client.download_file(self.s3_bucket, 'text/'+pic+"_label_analysis.json", 'text/'+pic+"_label_analysis.json")

        main = random.choice(game)
        shutil.copyfile('pics/'+main, 'pics/main_'+main)
        shutil.copyfile('text/'+main+"_facial_analysis.json", 'text/main_'+main+"_facial_analysis.json")
        shutil.copyfile('text/'+main+"_label_analysis.json", 'text/main_'+main+"_label_analysis.json")

    	print("--- Game Initialized! ---")


    def run(self):
        # print "Command new_game"
        # print self.options

        num_pics = int(self.options["<num_pics>"])
        if num_pics > 0:
            self.empty_game_dirs()
            self.initialize_game(num_pics)
        else:
            print "Number of pictures MUST be > 0"
